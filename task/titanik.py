import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df

def title_as(name):
    if 'Mr.' in name:
        return 'Mr.'
    if 'Mrs.' in name:
        return 'Mrs.'
    if 'Miss.' in name:
        return 'Miss'

def get_filled():
    df = get_titatic_dataframe()
    df['Title'] = df['Name'].apply(title_as)
    grouped = df.groupby(by=['Title'])

    res = []

    for title, data in grouped:
        mean_age = data['Age'].mean()
        missing_rec = data['Age'].isna().sum()
        res.append([title, mean_age, missing_rec])
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    return res
